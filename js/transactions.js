function loadTransactions() {

    let request = sendRequest('transaccion/list', 'GET', '');

    let tableHTML = document.getElementById("table-content");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach( row => {

            tableHTML.innerHTML += `
            
            <tr>
                        <td> ${ row.idTransaccion  } </td>
                        <td> ${ row.tipoTransaccion } </td>
                        <td> ${ row.fechaTransaccion } </td>
                        <td> ${ row.vendedor } </td>
                        <td> ${ row.comprador } </td>
                        <td class="col-right"> $ ${ row.total } </td>
                        <td>
                            <a href="/admin/transaccion/ver.html?id=${ row.idTransaccion }" class="btn btn-sm btn-success">
                                <i class="bi bi-eye"></i>
                            </a>
                            <a href="/admin/transaccion/editar.html?id=${ row.idTransaccion }" class="btn btn-sm btn-warning">
                                <i class="bi bi-pencil"></i>
                            </a> 
                            <a href="#" class="btn btn-sm btn-danger" data-bs-id="${ row.idTransaccion }"
                            data-bs-toggle="modal" data-bs-target="#deleteModal" >
                                <i class="bi bi-trash"></i>
                            </a>
                        </td>
            </tr>
            
            `;

        });

    }

    request.onerror = function(){
        
        let tableHTML = document.getElementById("table-content");
        tableHTML.innerHTML = "";

        tableHTML.innerHTML += `

        <tr>
            <td colspan="6">
                Error al cargar las transacciones
            </td>
        </tr>

        `;
    }


}

function loadTransactionById( id, readOnly ){

    let request = sendRequest( 'transaccion/list/'+id,'GET', '' );

    let idTransaccion = document.getElementById("idTransaccion");
    let tipoTransaccion = document.getElementById("tipoTransaccion");
    let fechaTransaccion = document.getElementById("fechaTransaccion");
    let vendedor = document.getElementById("vendedor");
    let comprador = document.getElementById("comprador");
    let total = document.getElementById("total");

    request.onload = function(){

        let data = request.response;

        if ( readOnly ){
            idTransaccion.innerHTML = data.idTransaccion;
            tipoTransaccion.innerHTML = data.tipoTransaccion;
            fechaTransaccion.innerHTML = data.fechaTransaccion;
            vendedor.innerHTML = data.vendedor;
            comprador.innerHTML = data.comprador;
            total.innerHTML = data.total;
        }else{
            idTransaccion.value = data.idTransaccion;
            tipoTransaccion.value = data.tipoTransaccion;
            fechaTransaccion.value = data.fechaTransaccion;
            vendedor.value = data.vendedor;
            comprador.value = data.comprador;
            total.value = data.total;
        }

    }

}


function saveTransaction(){

    let tipoTransaccion = document.getElementById("tipoTransaccion").value;
    let fechaTransaccion = document.getElementById("fechaTransaccion").value;
    let vendedor = document.getElementById("vendedor").value;
    let comprador = document.getElementById("comprador").value;
    let total = document.getElementById("total").value;


    let data = {
        'tipoTransaccion': tipoTransaccion,
        'fechaTransaccion': fechaTransaccion,
        'vendedor': vendedor,
        'comprador': comprador,
        'total': total
    }

    let request = sendRequest('transaccion/', 'POST', data);

    request.onload = function(){
        window.location = '/admin/transaccion/index.html';
    }

    request.onerror = function(){
        alert('Error al crear la transacción');
    }

}


function editTransaction(){

    let idTransaccion  = document.getElementById("idTransaccion").value;
    let tipoTransaccion = document.getElementById("tipoTransaccion").value;
    let fechaTransaccion = document.getElementById("fechaTransaccion").value;
    let vendedor = document.getElementById("vendedor").value;
    let comprador = document.getElementById("comprador").value;
    let total = document.getElementById("total").value;

    let data = {
        'idTransaccion': idTransaccion,
        'tipoTransaccion': tipoTransaccion,
        'fechaTransaccion': fechaTransaccion,
        'vendedor': vendedor,
        'comprador': comprador,
        'total': total
    }

    let request = sendRequest('transaccion/', 'PUT', data);

    request.onload = function(){
        window.location = '/admin/transaccion/index.html';
    }

    request.onerror = function(){
        alert('Error al editar la transaccion');
    }


}


function deleteTransaction( id ){

    let request = sendRequest('transaccion/'+id, 'DELETE', '');

    request.onload = function(){
        loadTransactions();
    }


}