function loadSelectFromDatabase(){

    let request = sendRequest('producto/list', 'GET', '');

    let selectHTML = document.getElementById("select-content");
    selectHTML.innerHTML += `<option value=""> Seleccione </option>`;

    request.onload = function () {

        let data = request.response;

        data.forEach( row => {

            selectHTML.innerHTML += `
            
            <option value="${ row.idProducto }"> ${ row.nombreProducto } </option>
            
            `;

        });

    }

    request.onerror = function(){
        
        let selectHTML = document.getElementById("select-content");
        selectHTML.innerHTML = "";
        selectHTML.innerHTML += `
        <option>Error al cargar los productos</option> 
        
        `;
    }

}